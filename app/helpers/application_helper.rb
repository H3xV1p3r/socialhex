module ApplicationHelper
    
    # Display Full Title 
    def full_title(page_title = '')
        title = "SocialH3x"
        if page_title.empty?
            title 
        else
            page_title + " | " + title 
        end
    end
    
    
end
